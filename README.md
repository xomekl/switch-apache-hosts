# README #

'Switch apache host' is a very small script which allows to change host destination. It is useful if you have ssd drive and had to use external one and don't want to use different host names with help, e.g. Dnsmasq.

### Using ###

```
#!c
saph
```
### Parameters ###

```
#!c
saph status
```
show what drive you have in apache config now.

### Who do I talk to? ###
As it is my first attempt in linux script any help and advices are appreciated, please use this email - [xomekl@yandex.ru](mailto:xomekl@yandex.ru)